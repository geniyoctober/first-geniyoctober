from .base import *

DEBUG = int(os.environ.get('DEBUG', default=0))

ADMINS = (
    ('admin', os.environ.get('ADMIN_EMAIL')),
)

ALLOWED_HOSTS = os.environ.get('DJANGO_ALLOWED_HOSTS').split(" ")

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME': os.environ.get('POSTGRES_DB'),
       'USER': os.environ.get('POSTGRES_USER'),
       'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
       'HOST': 'db',
       'PORT': 5432,
   }
}

#SECURE_SSL_REDIRECT = True
#CSRF_COOKIE_SECURE = True
