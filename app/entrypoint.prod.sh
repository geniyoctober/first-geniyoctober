#!/bin/sh
echo 'Run migration'
python3 manage.py makemigrations
python3 manage.py migrate
echo 'Collect Staticc'
python3 manage.py collectstatic --noinput
python3 manage.py test
exec "$@"
