#!/bin/sh

echo DEBUG=0 >> .env
echo SECRET_KEY=$SECRET_KEY >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo CI_PROJECT_NAMESPACE=$CI_PROJECT_NAMESPACE  >> .env
echo CI_PROJECT_NAME=$CI_PROJECT_NAME  >> .env
echo CI_BUILD_REF_NAME=${CI_BUILD_REF_NAME}  >> .env
echo CI_BUILD_TOKEN=$CI_BUILD_TOKEN  >> .env
echo CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG  >> .env
echo CI_PIPELINE_ID=$CI_PIPELINE_ID  >> .env
echo ADMIN_EMAIL='geniyoctober@yandex.ru' >> .env
echo DJANGO_ALLOWED_HOSTS=$DEPLOY_ADDR >> .env
echo POSTGRES_USER='postgres'  >> .env
echo POSTGRES_DB='postgres'  >> .env
echo POSTGRES_PASSWORD='password'  >> .env
echo WEB_IMAGE=$IMAGE:web  >> .env
echo NGINX_IMAGE=$IMAGE:nginx  >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env
echo DEPLOY_ADDR=$DEPLOY_ADDR >> .env
